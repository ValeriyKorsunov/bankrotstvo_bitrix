<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');


CHTTP::SetStatus("Ошибка 404! Страница не найдена!");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Ошибка 404. Страница не найдена");
$APPLICATION->SetTitle("Ошибка 404! Страница не найдена!");
?><section class="section">
<div class="container container_xs text_container">
	<h1 class="section__title">Ошибка 404</h1>
	<div class="text">
		<p>
			 К сожалению страница не найдена
		</p>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.map",
			"map",
			Array(
				"CACHE_TIME" => "3600",
				"CACHE_TYPE" => "A",
				"COL_NUM" => "1",
				"LEVEL" => "3",
				"SET_TITLE" => "Y",
				"SHOW_DESCRIPTION" => "N"
			)
		);?>
	</div>
	 <!-- /.container --><br>
<br>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>