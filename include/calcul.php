<? CModule::IncludeModule("iblock"); // для работы с инфоблоком ?>
<section class="section section_bg_gray calculator" id="calculator">
    <div class="calculator__list js-calculator-slider">

        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item active"></li>
                    <li class="steps__item"></li>
                    <li class="steps__item"></li>
                </ol>

                <div class="form__row row">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">В каком регионе зарегистрирован должник:</label>
                            </div>
                            <?$res = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE"=>'region'), false, Array(), Array()); //получить список элементов - регионы ?>
                            <select name="REG" class="js-select">
                                <?php $ferst = $res->GetNextElement()->GetFields();?>
                                <option selected><?=$ferst['NAME']?></option>
                                <?php while($ob = $res->GetNextElement()):?>
                                    <?$arFields = $ob->GetFields();?>
                                    <option><?=$arFields['NAME']?></option>
                                <?endwhile;?>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group form__group_centered">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Является ли должник <br>пенсионером по старости
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Подсказка
                                        </div>
                                    </div>
                                </label>
                            </div>

                            <div class="form__choice">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control__input" name="old" value="0" checked>
                                    <div class="custom-control__indicator"></div>
                                    Нет
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control__input" name="old" value="1">
                                    <div class="custom-control__indicator"></div>
                                    Да
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Количество иждевенцев (несовершеннолетних детей):</label>
                            </div>
                            <select name="IZHD" data-hide="izhd" class="js-select">
                                <option selected>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form__row row">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Сумма долга:</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-1"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-1">1500000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Ежемесячный доход:</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-2"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-2">100000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 invise" id="izhd">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Размер выплаты по <br>алиментам (если имеются):</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-3"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-3">50000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary button_md js-calculator-slider-next" id="region_save">Далее</button>
                </div>
            </div><!-- /.container -->
        </div>

        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item"></li>
                    <li class="steps__item active"></li>
                    <li class="steps__item"></li>
                </ol>

                <div class="form__row row">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Транспорт находящийся в собственности должника:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="TRANS" data-hide="trans" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Общее количество залогового недвижимости:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="NEDVZ" data-hide="nedvz" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Недвижимое имущество в собственности должника:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="NEDV" data-hide="nedv" class="js-select">
                                <option  value="0" selected hidden>Нет</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form__row row">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 invise" id="trans">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Общая стоимость:</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-4"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-4">1 500 000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 invise" id="nedvz">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Его общая стоимость:</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-5"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-5">1 500 000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 invise" id="nedv">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">Общая стоимость <br>недвижимого имущества:</label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-6"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-6">1 500 000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Общее количество сделок по продаже:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="SDEL" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary_outline button_md js-calculator-slider-prev">Назад</button>
                    <button class="form__button button button_primary button_md js-calculator-slider-next">Далее</button>
                </div>
            </div><!-- /.container -->
        </div>

        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item"></li>
                    <li class="steps__item"></li>
                    <li class="steps__item active"></li>
                </ol>

                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group form__group_centered">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Должник находится в браке
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form__choice">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control__input" name="married" value="0" checked id="holost">
                                    <div class="custom-control__indicator"></div>
                                    Нет
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control__input" name="married" value="1" id="brak">
                                    <div class="custom-control__indicator"></div>
                                    Да
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="opacity">
                    <div class="form__row row">
                        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">
                                        Является ли должник или его супруг учредителем юр.:
                                        <div class="tooltip tooltip_right">
                                            <div class="tooltip__toggle">?</div>
                                            <div class="tooltip__body">
                                                <a href="#" class="tooltip__close icon-close"></a>
                                                Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <select name="ORG" class="js-select">
                                    <option  value="0" selected hidden>Выберите из списка</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">
                                        Недвижимое имущество в собственности супруга:
                                        <div class="tooltip tooltip_right">
                                            <div class="tooltip__toggle">?</div>
                                            <div class="tooltip__body">
                                                <a href="#" class="tooltip__close icon-close"></a>
                                                Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <select name="NEDVS" class="js-select">
                                    <option value="0" selected hidden>Выберите из списка</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">
                                        Транспорт находящийся в собственности супруга:
                                        <div class="tooltip tooltip_right">
                                            <div class="tooltip__toggle">?</div>
                                            <div class="tooltip__body">
                                                <a href="#" class="tooltip__close icon-close"></a>
                                                Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <select name="TRANSS" class="js-select">
                                    <option value="0" selected hidden>Выберите из списка</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form__row row">
                        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">
                                        Общее количество сделок по продаже:
                                        <div class="tooltip tooltip_right">
                                            <div class="tooltip__toggle">?</div>
                                            <div class="tooltip__body">
                                                <a href="#" class="tooltip__close icon-close"></a>
                                                Общее количество сделок по продаже, дарению недвижимости и/или транспорта, совершенных должником за последние три года (потенциально оспоримые): (сюда подподают сделки с заниженной рыночной стоимостью, сделки с близкими родственниками) (не подподают сделки совершенные до возникновения задолженности, или например до получения кредита). Сделки совершенные супругом, с аналогичными условиями совершения.
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <select name="SDELS" class="js-select">
                                    <option value="0" selected hidden>Выберите из списка</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form__col col-xs-12 col-md-4 col-md-bottom">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">Общая стоимость такого <br>имущества:</label>
                                </div>
                                <div class="form__range-slider range-slider-block">
                                    <div class="range-slider js-range-slider-7"></div>
                                    <div class="range-slider__data">
                                        <span class="js-range-amount-7">50 000</span>&nbsp;&#8381;
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form__col col-xs-12 col-md-4 col-md-bottom">
                            <div class="form__group">
                                <div class="form__label-wrap">
                                    <label class="form__label">Общая стоимость:</label>
                                </div>
                                <div class="form__range-slider range-slider-block">
                                    <div class="range-slider js-range-slider-8"></div>
                                    <div class="range-slider__data">
                                        <span class="js-range-amount-8">50 000</span>&nbsp;&#8381;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary_outline button_md js-calculator-slider-prev">Назад</button>
                    <input type="submit" class="form__button button button_primary button_md  js-calculator-slider-next" id="js-calculate-ajax" value="Рассчитать">
                    <!--                    <input type="submit" class="form__button button button_primary button_md  js-calculate-ajax" value="Рассчитать"> -->
                    <!--                    <button class="form__button button button_primary button_md js-calculator-slider-next">Далее</button> -->
                </div>
            </div><!-- /.container -->
        </div>

        <!-- Результат -->
        <div class="calculator__item">
            <div class="container container_md result">


            </div>
        </div>

    </div>
</section>


