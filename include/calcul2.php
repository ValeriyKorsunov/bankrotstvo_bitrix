<? 
CModule::IncludeModule("iblock"); // для работы с инфоблоком 
require_once $_SERVER['DOCUMENT_ROOT'].'/calculator_set.php';
?>
<section class="section section_bg_gray calculator" id="calculator">
    <div class="calculator__list js-calculator-slider">
        <!--step 1-->
        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item active"></li>
                    <li class="steps__item"></li>
                    <li class="steps__item"></li>
                </ol>
                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    В каком регионе зарегистрирован<br>должник
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно выбрать регион в котором Вы зарегистрированы по месту жительства
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <?$res = CIBlockElement::GetList(Array("SORT"=>"DESC", "NAME"=>"ASC"), Array("IBLOCK_CODE"=>'region'), false, Array(), Array()); //получить список элементов - регионы ?>
                            <select name="REG" class="js-select">
                                <?php $ferst = $res->GetNextElement()->GetFields();?>
                                <option selected><?=$ferst['NAME']?></option>
                                <?php while($ob = $res->GetNextElement()):?>
                                    <?$arFields = $ob->GetFields();?>
                                    <option><?=$arFields['NAME']?></option>
                                <?endwhile;?>
                            </select>
                        </div>
                    </div>
                    <?/*div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Ежемесячный доход
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Необходимо указать сумму Вашего ежемесячного совокупного официального дохода (заработной платы, пенсии)
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-2"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-2">100000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div*/?>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Недвижимость в собственности за исключением единственного жилья:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество единиц недвижимого имущества (жилье, гаражи, земельные участки, нежилые помещения) находящегося в Вашей собственности, за исключением того в котором Вы проживаете.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="NEDV" data-hide="nedv" class="js-select">
                                <option  value="0" selected hidden>Нет</option>
                                <?for($i=1;$i <= $colNedvig; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Транспорт находящийся в собственности должника:
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество единиц транспорта (автомобили легковые, грузовые, мотоциклы, мопеды) находящиеся в Вашей собственности.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="TRANS" data-hide="trans" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <?for($i=1;$i <= $colTrans; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Сумма долга
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Необходимо указать все долги независимо от просрочки
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <input type="text" id="summa" NAME="SUMMA" class="input__text" value="300000"> 
                            <?/*div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-1"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-1">1500000</span>&nbsp;&#8381;
                                </div>
                            </div*/?>
                        </div>
                    </div>
                    <?/*div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Количество иждевенцев (несовершеннолетних детей)
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Если у Вас имеются несовершеннолетние дети, кроме детей в пользу которых оплачиваются алименты, необходимо указать количество
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select class="js-select">
                                <option selected>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Размер выплаты по <br>алиментам (если имеются)
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Если Вы являетесь плательщиком алиментов, то необходимо указать общую сумму которую Вы обязаны оплачивать
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form__range-slider range-slider-block">
                                <div class="range-slider js-range-slider-3"></div>
                                <div class="range-slider__data">
                                    <span class="js-range-amount-3">50000</span>&nbsp;&#8381;
                                </div>
                            </div>
                        </div>
                    </div*/?>
                </div>
                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary button_md js-calculator-slider-next" id="region_save">Далее</button>
                </div>
            </div><!-- /.container -->
        </div>
        <!--step 2-->
        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item"></li>
                    <li class="steps__item active"></li>
                    <li class="steps__item"></li>
                </ol>

                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Совершались ли Вами за последние 3 года сделки (продажа, дарение имущества)
                                </label>
                            </div>
                            <select name="sdelki" data-hide="sdelki" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <option value="1">Да</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 sdelki invise">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Количество сделок с недвижимым имуществом :
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество сделок.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="sdelkiNDI" data-deal="NDI" class="js-select"  id="amountDeals">
                                <option value="0" selected hidden>0</option>
                                <?for($i=1;$i <= 6; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 sdelki invise">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Количество сделок с движимым имуществом :
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество сделок.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="sdelkiDI" data-deal="DI" class="js-select"  id="amountDeals">
                                <option value="0" selected hidden>0</option>
                                <?for($i=1;$i <= 6; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="sdelkiNDI"></div>
                <div id="sdelkiDI"></div>
                
                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary_outline button_md js-calculator-slider-prev">Назад</button>
                    <button class="form__button button button_primary button_md js-calculator-slider-next">Далее</button>
                </div>
            </div><!-- /.container -->
        </div>
        <!--step 3-->
        <div class="calculator__item">
            <div class="container container_md">
                <h2 class="section__title">РАСЧЕТ СТОИМОСТИ БАНКРОТСТВА</h2>
                <p class="section__subtext">С помощью нашего калькулятора, вы можете самостоятельно произвести расчет услуг</p>
                <ol class="calculator__steps steps">
                    <li class="steps__item"></li>
                    <li class="steps__item"></li>
                    <li class="steps__item active"></li>
                </ol>
                <div class="form__row row row-lg-justify-center">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-5">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Находитесь в браке
                                </label>
                            </div>
                            <select name="married" class="js-select" data-hide="obshee">
                               <option value="0" selected>Нет или расторгнут более 3 лет назад</option>
                               <option value="1">В браке</option>
                               <option value="1">Расторгнут менее 3ех лет назад</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 invise obshee">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Общее имущество супругов
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Укажите имеется ли зарегистрированное на супруге имущество, приобретенное во время брака
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="estateMarried" data-hide="sdelki2" class="js-select">
                                <option value="0" selected hidden>Нет</option>
                                <option value="1">Да</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form__row row row-lg-justify-center invise sdelki2">
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Недвижимость
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество единиц недвижимого имущества (жилье, гаражи, земельные участки, нежилые помещения) приобретенного во время брака и зарегистрированного на супруга
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="realEstateSpouse" class="js-select" data-hide="obshee">
                               <option value="0" selected>Нет</option>
                               <option>1</option>
                               <option>2</option>
                               <option>3</option>
                               <option>4</option>
                               <option>5</option>
                               <option>6</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Транспорт
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество единиц транспорта (автомобили легковые, грузовые, мотоциклы, мопеды)  приобретенного во время брака  и зарегистрированного на супруга
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="vehicleInMarried" class="js-select" data-hide="obshee">
                               <option value="0" selected>Нет</option>
                               <option>1</option>
                               <option>2</option>
                               <option>3</option>
                               <option>4</option>
                               <option>5</option>
                               <option>6</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Совершались ли супругом сделки с общим имуществом за последние три года
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                                ...
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="sdelkiS" class="js-select" data-hide="sdelkiS">
                               <option value="0" selected>Нет</option>
                               <option value="1">Да</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 sdelkiS invise">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Количество сделок с недвижимым имуществом :
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество сделок.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="sdelkiNDIS" data-deal="NDIS" class="js-select" id="amountDeals">
                                <option value="0" selected hidden>0</option>
                                <?for($i=1;$i <= 6; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                    <div class="form__col col-xs-12 col-sm-6 col-lg-4 sdelkiS invise">
                        <div class="form__group">
                            <div class="form__label-wrap">
                                <label class="form__label">
                                    Количество сделок с движимым имуществом :
                                    <div class="tooltip tooltip_right">
                                        <div class="tooltip__toggle">?</div>
                                        <div class="tooltip__body">
                                            <a href="#" class="tooltip__close icon-close"></a>
                                            Нужно указать количество сделок.
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <select name="sdelkiDIS" data-deal="DIS" class="js-select" id="amountDeals">
                                <option value="0" selected hidden>0</option>
                                <?for($i=1;$i <= 6; $i++):?>
                                    <option><?=$i?></option>
                                <?endfor;?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div id="sdelkiNDIS"></div>
                <div id="sdelkiDIS"></div>
                
                <div class="form__group form__group_submit">
                    <button class="form__button button button_primary_outline button_md js-calculator-slider-prev">Назад</button>
                    <input type="submit" class="form__button button button_primary button_md  js-calculator-slider-next" id="js-calculate-ajax" value="Рассчитать">

<!--                    <button class="form__button button button_primary_outline button_md js-calculator-slider-prev">Назад</button>
                    <button class="form__button button button_primary button_md js-calculator-slider-next">Далее</button> -->
                </div>
            </div><!-- /.container -->
        </div>
        <!-- Результат -->
        <div class="calculator__item">
            <div class="container container_md result">


            </div>
        </div>

    </div>
</section>
