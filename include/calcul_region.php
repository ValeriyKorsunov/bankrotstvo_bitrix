<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
?>
<?
$region = $_POST['region'];
$ses_ID = bitrix_sessid();

if(isset($_POST['region'])){

    add_ement_block($region,$ses_ID);
}


function add_ement_block($region,$ses_ID){
    $typiblocks = CIBlockType::GetList(array(),array('code'=>'calcul_region_type'));

    if ($typiblocks->Fetch()){
        //проверка на наличие ИБ
        $iblocks = CIBlock::GetList(array(),array('TYPE'=>'calcul_region_type'));
        $arblocks = $iblocks->Fetch();
        if($arblocks['NAME'] == "Регистрация региона и сессии"){
            // проверка существования записи текущей сессии
            $arFilter = Array("IBLOCK_ID"=>$arblocks["ID"],'PREVIEW_TEXT' =>$ses_ID );
            $items = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), Array());
            if($items->Fetch()){
                //echo 'Город уже добавлен';
            }
            else{

                // добавить элемент ИБ (записать сообщение)
                //print_mi($_POST);
                $el = new CIBlockElement;
                $arLoadProductArray = Array(
                    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                    "IBLOCK_ID" => $arblocks["ID"],
                    "NAME" => $ses_ID,
                    "ACTIVE" => "Y",            // активен
                    "PREVIEW_TEXT" => $region,
                    "DETAIL_TEXT" => ""
                );
                $PRODUCT_ID = $el->Add($arLoadProductArray);
                if (!$PRODUCT_ID) {
                    echo 'Error: ' . $el->LAST_ERROR . '<br>';
                    return false;
                }
            }
        }
    }
}
?>
