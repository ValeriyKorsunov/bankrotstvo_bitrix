<?php
/**
 * User: korsunov
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
// Настройки расчета
require_once $_SERVER['DOCUMENT_ROOT'].'/calculator_set.php';


$X = (int)preg_replace("/[^\d]+/", "", $_POST['X']);                            // Сумма долга 
$ALIM = (int)preg_replace("/[^\d]+/", "", $_POST['ALIM']);                      // Размер выплаты по алиментам (если имеются)(руб.)
$DMES = (int)preg_replace("/[^\d]+/", "", $_POST['DMES']);                      // Ежемесячный доход должника 
$STN = (int)preg_replace("/[^\d]+/", "", $_POST['STN']);                        // Общая стоимость недвижимого имущества (в собственности) 
$STNZ = (int)preg_replace("/[^\d]+/", "", $_POST['STNZ']);                      // общая стоимость залогового недвижимого имущества (в том числе ипотека) (руб.)
$STNS = (int)preg_replace("/[^\d]+/", "", $_POST['STNS']);                      // Общая стоимость такого имущества в собственности супруга должника
$STR = (int)preg_replace("/[^\d]+/", "", $_POST['STR']);                        // Общая стоимость транспорта
$STRS = (int)preg_replace("/[^\d]+/", "", $_POST['STRS']);                      // Общая стоимость транспорта в собственности супруга
$regionNotWork = '';                                                                                //Сообщение о нерабочем регионе

// 1.
    if( $X < $limitCredit)
    {
        $title = 'Проведение процедуры банкротства невозможно.';
        $text_lg = 'Ваша сумма долга не достаточна для проведения процедуры банкротства.';
        bankruptcyProceedingsOff($title, $text_lg);
    }
    switch (true) 
    {
        case ($limitCredit <= $X && $X < $limitCredit_1):
            $baseCost -= $coefficient_1;
            break;
        case ($limitCredit_1 <= $X && $X < $limitCredit_2):
            $baseCost -= $coefficient_2;
            break;
        case ($limitCredit_2 <= $X && $X < $limitCredit_3):
            $baseCost -= $coefficient_3;
            break;    
        case ($limitCredit_3 <= $X && $X < $limitCredit_4):
            $baseCost -= $coefficient_4;
            break;
        case ($limitCredit_4 <= $X && $X < $limitCredit_5):
            $baseCost -= $coefficient_5;
            break;
        case ($limitCredit_6 <= $X):
            $baseCost += $X*($coefficient_6/100);
            break;
    }

// 2.
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_CODE"=>'region', 'NAME'=>$_POST['REG']), false, false, Array());
    $arr_res = $res->Fetch();
    // получить свойство элемента
    $db_props = CIBlockElement::GetProperty($arr_res['IBLOCK_ID'], $arr_res['ID'], Array(), Array('CODE'=>'REG_DOPLATA'));
    $ar_props = $db_props->Fetch();
    $reg_doplata = $ar_props['VALUE'];                                          // РАЗМЕР ДОПЛАТЫ ЗА РЕГИОН
    $baseCost += $reg_doplata; 
    if($arr_res['ACTIVE'] == 'N')
    {
        $regionNotWork = 'Услуги не оказываются в данном регионе';
    }
    //PHPDebug::dump($arr_res);
// 6. Недвижимость в собственности за исключением единственного жилья
        $baseCost +=  $_POST['NEDV']*$NNEDV;

// 7. Транспорт 
    if ($_POST['TRANS'] > 3) {
        $baseCost +=  ($_POST['TRANS']-3)*$NTRANS;
    }

// 8. за последние три года сделки
    // недвижемое имущество
    if ($_POST['sdelkiNDI'] != '0')
    {
        foreach ($_POST['type_transaction']['NDI'] as $key => $value)
        {
            if($value == '0')
            {
                if($_POST['debts_time_transaction']['NDI'][$key] == '1' 
                    && ($_POST['below_market_price']['NDI'][$key]=='0' || $_POST['buyer_relative']['NDI'][$key]=='1') )
                {
                    $baseCost += $transactionContested;
                }
            }
            if($value=='1')
            {
                if($_POST['debt_moment_donation']['NDI'][$key]=='1' && $_POST['gift_relative']['NDI'][$key]=='1')
                {
                    $baseCost += $transactionContested;
                }
            }
        }
    }
    // движемое имущество
    if($_POST['sdelkiDI'] != '0')
    {
        foreach ($_POST['type_transaction']['DI'] as $key => $value)
        {
            if($value == '0')
            {
                if($_POST['debts_time_transaction']['DI'][$key] == '1' 
                    && ($_POST['below_market_price']['DI'][$key]=='0' || $_POST['buyer_relative']['DI'][$key]=='1') )
                {
                    $baseCost += $transactionContestedTrans;
                }
            }
            if($value=='1')
            {
                if($_POST['debt_moment_donation']['DI'][$key]=='1' && $_POST['gift_relative']['DI'][$key]=='1')
                {
                    $baseCost += $transactionContestedTrans;
                }
            }
        }
    }

// 9. 
    if ($_POST['married'] == '1' && $_POST['estateMarried'] == '1')
    {
        $baseCost += $_POST['realEstateSpouse'] * $increaseRealEstateSpouse;
        if ($_POST['vehicleInMarried'] > 3) {
            $baseCost += ($_POST['vehicleInMarried']-3)*$vehicleInMarried;
        }
        if($_POST['sdelkiS'] == '1')
        {
            // недвижемое имущество
            if ($_POST['sdelkiNDIS'] != '0')
            {
                foreach ($_POST['type_transaction']['NDIS'] as $key => $value)
                {
                    if($value == '0')
                    {
                        if($_POST['debts_time_transaction']['NDIS'][$key] == '1' 
                            && ($_POST['below_market_price']['NDIS'][$key]=='0' || $_POST['buyer_relative']['NDIS'][$key]=='1') )
                        {
                            $baseCost += $transactionContested;
                        }
                    }
                    if($value=='1')
                    {
                        if($_POST['debt_moment_donation']['NDIS'][$key]=='1' && $_POST['gift_relative']['NDIS'][$key]=='1')
                        {
                            $baseCost += $transactionContested;
                        }
                    }
                }
            }
            // движемое имущество
            if($_POST['sdelkiDIS'] != '0')
            {
                foreach ($_POST['type_transaction']['DIS'] as $key => $value)
                {
                    if($value == '0')
                    {
                        if($_POST['debts_time_transaction']['DIS'][$key] == '1' 
                            && ($_POST['below_market_price']['DIS'][$key]=='0' || $_POST['buyer_relative']['DIS'][$key]=='1') )
                        {
                            $baseCost += $transactionContestedTrans;
                        }
                    }
                    if($value=='1')
                    {
                        if($_POST['debt_moment_donation']['DIS'][$key]=='1' && $_POST['gift_relative']['DIS'][$key]=='1')
                        {
                            $baseCost += $transactionContestedTrans;
                        }
                    }
                }
            }
        }
    }
    
// 
?>
<?
// Банкротсво невозможно
    function bankruptcyProceedingsOff($title, $text_lg)
    {
    ?>
        <h2 class="section__title">
            <?=$title?>
        </h2>
        <div class="calculator__text text text_lg">
            <?=$text_lg?>
        </div>

        <?contacts()?>
        
        <?reloadCalc()?>
    <?
    }
//    
?>


<?php

    function contacts()
    {
    ?>
        <div class="form__row row row-xs-justify-center">
            <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                <a href="tel:+78005501807" class="tel h2">8 (800) 550-18-07</a>
                <div class="form__note">
                    Для получения подробной информации, свяжитесь с нами по телефону.
                </div>
            </div>
        </div>
    <?    
    }
    
    function reloadCalc(){
    ?>
        <div class="form__row row row-xs-justify-center">
            <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                <button class="form__button button button_primary button_md" id="reload">Пересчитать</button>
                <div class="form__note">
                    Чтобы пересчитать итоговую стоимость, кликните по кнопке "Пересчитать".
                </div>
            </div>
        </div>
    <?    
    }
    //расчет рассрочки
    function installmentPlan($baseCost,$num)
    {
        $payment = 12500;
    ?>
        <table border="1">
            <tr>
                <td>Пример рассрочки на <?=$num?> месяцев </td>
            </tr>
            <tr>
                <td>1 платеж – <?=$payment?>р. </td>
            </tr>
            <tr>
                <td>2 платеж – <?=$payment?>р. </td>
            </tr>
            <? $othrrPayments = round(($baseCost-$payment*2)/($num-2),-2); ?>
            <?for($i=3;$i<$num;$i++):?>
                <tr>
                    <td><?=$i?> платеж – <?=$othrrPayments?>р. </td>
                </tr>
            <?endfor;?>
        </table>
    <?
    }
?>

<?if($X > $limitCredit):?>
    <?if($regionNotWork === ''):?>
        <h2 class="section__title">
            СТОИМОСТЬ ПРОЦЕДУРЫ СОСТАВИТ
        </h2>
        <div class="form__row row row-xs-justify-center">
            <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                <div class="calculator__result h2">
                    <? echo number_format($baseCost,0,'',' ');?> руб.
                </div>
            </div>
        </div>
    <?endif;?>
    <?if ($regionNotWork !== ''):?>
        <div class="calculator__text text text_lg">
            <? echo $regionNotWork; ?>
        </div>
    <?endif;?>
    <?
    if(TRUE)
    {
        /*echo '<div style="display: table;">';
        echo '<div style="display: table-cell;">'; installmentPlan($baseCost, 5); echo '</div>';
        echo '<div style="display: table-cell;">'; installmentPlan($baseCost, 7);echo '</div>';
        echo '<div style="display: table-cell;">'; installmentPlan($baseCost, 10);echo '</div>';
        echo '<div style="display: table-cell;">'; installmentPlan($baseCost, 12);echo '</div>';
        echo '</div>';*/
    }
    ?>

    <?contacts()?>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="#feedback" class="form__button button button_primary button_md">Написать</a>
            <div class="form__note">
                Заполните форму для того,  чтобы наши сотрудники связались с вами.
            </div>
        </div>
    </div>

    <?reloadCalc()?>

<?endif;?>

