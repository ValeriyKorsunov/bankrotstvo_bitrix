<?php
/**
 * User: korsunov
 */

 
require_once $_SERVER['DOCUMENT_ROOT'].'/calculator_set.php';

$ALIM = (int)preg_replace("/[^\d]+/", "", $_POST['ALIM']);                      // Размер выплаты по алиментам (если имеются)(руб.)
$X = (int)preg_replace("/[^\d]+/", "", $_POST['X']);                            // Сумма долга 
$DMES = (int)preg_replace("/[^\d]+/", "", $_POST['DMES']);                      // Ежемесячный доход должника 
$STN = (int)preg_replace("/[^\d]+/", "", $_POST['STN']);                        // Общая стоимость недвижимого имущества (в собственности) 
$STNZ = (int)preg_replace("/[^\d]+/", "", $_POST['STNZ']);                      // общая стоимость залогового недвижимого имущества (в том числе ипотека) (руб.)
$STNS = (int)preg_replace("/[^\d]+/", "", $_POST['STNS']);                      // Общая стоимость такого имущества в собственности супруга должника
$STR = (int)preg_replace("/[^\d]+/", "", $_POST['STR']);                        // Общая стоимость транспорта
$STRS = (int)preg_replace("/[^\d]+/", "", $_POST['STRS']);                      // Общая стоимость транспорта в собственности супруга


require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
// *****************************************************************************

$C;     // Цена
$B = 0; //Базовая ставка
// Базовая ставка рассчитывается из суммы долга
if ($minBSdo >= $X && $X > $minBSot) {
    $B = ($X - $minBSot) / 10 + $BSmin;
}
elseif ($srBSdo >= $X && $X > $minBSdo){
    $B=$BS;
}
elseif ($X > $srBSdo){
    $B=$BS+$kBSsv*$X;
}
// Надбавки
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_CODE"=>'region', 'NAME'=>$_POST['REG']), false, false, Array());
$arr_res = $res->Fetch();
// получить свойство элемента
$db_props = CIBlockElement::GetProperty($arr_res['IBLOCK_ID'], $arr_res['ID'], Array(), Array('CODE'=>'REG_DOPLATA'));
$ar_props = $db_props->Fetch();
$reg_doplata = $ar_props['VALUE'];                                          // РАЗМЕР ДОПЛАТЫ ЗА РЕГИОН

// Надбавки рассчитываются по формуле N= Reg+ 20000*Sdel+3000*Trans+7000Nedv+3000Org
if ($_POST['married']=='0') 
{
	$N = $reg_doplata + $NSDEL * $_POST['SDEL'] + $NTRANS * $_POST['TRANS'] + $NNEDV * $_POST['NEDV'] + $NORG * $_POST['ORG'];
}
elseif ($_POST['married']=='1') {
    $N = $reg_doplata + $NSDEL * $_POST['SDEL'] + $NTRANS * ($_POST['TRANS']+$_POST['TRANSS']) + $NNEDV * ($_POST['NEDV']+$_POST['NEDVS']) + $NORG * $_POST['ORG'];
}
// Сумма официального дохода с учетом всех выплат --- Dox=Min-Alim-Mini*Izhd
$DOX = $MIN - $ALIM - $MIN*$_POST['IZHD'];  // Думаю целесообразно добавить $DMES - есл это больше $MIN

$kolmes = floor($X/$DOX);

// если холост или нет
if ($_POST['married']=='0'){
    $SIM = $STN + $STNZ + $STR;         // общая стоимость имущества
}
elseif ($_POST['married']=='1') {
    $SIM = $STN + $STNZ + $STNS + $STR + $STRS;
}

$C = 0;                                                                         // Цена
if ($DMES<=0){
    $C = floor((1-$Ss)*($B+$N-$S));
}elseif ($DMES>0) {
    $C = floor((1-$Ss)*($B+$N));
}

//echo 'B= '.$B.'<br> N= '.$N.'<br>';
?>
<?if($kolmes<12):?>
    <h2 class="section__title">
        БАНКРОТСТВО ..... .
    </h2>

    <div class="calculator__text text text_lg">
        Есть риск, что будет введена процедура реструктуризации долгов.
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="tel:+7999999999" class="tel h2">+7 (999) 99-99-99</a>
            <div class="form__note">
                Для получения подробной информации, свяжитесь с нами по телефону.
            </div>
        </div>
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <button class="form__button button button_primary button_md" id="reload">Пересчитать</button>
            <div class="form__note">
                Чтобы пересчитать итоговую стоимость, кликните по кнопке "Пересчитать".
            </div>
        </div>
    </div>
<?elseif($SIM >= $X):?>
    <h2 class="section__title">
        БАНКРОТСТВО НЕ ВОЗМОЖНО.
    </h2>
    
    <div class="calculator__text text text_lg">
        В вашем случае банкротство невозможно, так как общая стоимость имущества больше суммы долга.
    </div>
    
    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="tel:+7999999999" class="tel h2">+7 (999) 99-99-99</a>
            <div class="form__note">
                Для получения подробной информации, свяжитесь с нами по телефону.
            </div>
        </div>
    </div>
    
    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <button class="form__button button button_primary button_md" id="reload">Пересчитать</button>
            <div class="form__note">
                Чтобы пересчитать итоговую стоимость, кликните по кнопке "Пересчитать".
            </div>
        </div>
    </div>

<?elseif($SIM < $X && $X-$SIM < 300000):?>

    <h2 class="section__title">
        СУММА ДОЛГА МЕНЬШЕ 100 000 ₽ <br>НЕТ ДОХОДА, НЕТ ИМУЩЕСТВА
    </h2>
    
    <div class="calculator__text text text_lg">
        В вашем случае банкротство нецелесообразно, так как расходы, связанные с ведением дела о банкротстве несоразмерны с суммой задолженности
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="tel:+7999999999" class="tel h2">+7 (999) 99-99-99</a>
            <div class="form__note">
                Для получения подробной информации, свяжитесь с нами по телефону.
            </div>
        </div>
    </div>
    
    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <button class="form__button button button_primary button_md" id="reload">Пересчитать</button>
            <div class="form__note">
                Чтобы пересчитать итоговую стоимость, кликните по кнопке "Пересчитать".
            </div>
    </div>
</div>

<?else:?>
    <h2 class="section__title">
        СТОИМОСТЬ ПРОЦЕДУРЫ СОСТАВИТ
    </h2>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <div class="calculator__result h2">
                <? echo $C;?> руб.
            </div>
        </div>
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="tel:+7999999999" class="tel h2">+7 (999) 99-99-99</a>
            <div class="form__note">
                Для получения подробной информации, свяжитесь с нами по телефону.
            </div>
        </div>
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <a href="#feedback" class="form__button button button_primary button_md">Написать</a>
            <div class="form__note">
                Заполните форму для того,  чтобы наши сотрудники связались с вами.
            </div>
        </div>
    </div>

    <div class="form__row row row-xs-justify-center">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <button class="form__button button button_primary button_md" id="reload">Пересчитать</button>
            <div class="form__note">
                Чтобы пересчитать итоговую стоимость, кликните по кнопке "Пересчитать".
            </div>
        </div>
    </div>

<?endif;?>