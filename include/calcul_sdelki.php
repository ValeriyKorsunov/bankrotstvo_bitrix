<?php
/*
 * Для ответа на ajax запрос сделки за последние 3-и года
 *   */
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$sd = $_POST['typeSd'];
?>

<?if ($sd == 'NDI' || $sd == 'NDIS'):?>
    <p><b>Сделки с недвижимым имуществом:</b></p>
<? endif;?>
 
<?if ($sd == 'DI' || $sd == 'DIS'):?>
    <p><b>Сделки с движимым имуществом:</b></p>
<? endif;?>
    
<?for($x=0; $x<$_POST["sdelki"];$x++):?>
    <div class="form__row row">
        <div class="form__col col-xs-12 col-sm-6 col-lg-4">
            <div class="form__group form__group_centered">
                    <div class="form__label-wrap">
                        <label class="form__label">
                                Вид сделки:
                        </label>
                    </div>
                    <div class="form__choice">
                        <label class="custom-control custom-radio" data-check="prodazha_toggle_<?=$x?>_<?=$sd?>"  data-checked="prodazha_<?=$x?>_<?=$sd?>">
                            <input type="radio" class="custom-control__input" name="type_transaction[<?=$sd?>][<?=$x?>]" value="0" checked>
                                <div class="custom-control__indicator"></div>
                                Продажа
                        </label>
                            <label class="custom-control custom-radio" data-check="prodazha_toggle_<?=$x?>_<?=$sd?>"  data-checked="dar_<?=$x?>_<?=$sd?>">
                                <input type="radio" class="custom-control__input" name="type_transaction[<?=$sd?>][<?=$x?>]" value="1">
                                    <div class="custom-control__indicator"></div>
                                    Дарение
                            </label>
                    </div>
            </div>
        </div>
        <div class="sdelki prodazha_toggle_<?=$x?>_<?=$sd?>  col-xs-8 col-sm-8 col-lg-8 container pb0 pt0" id="prodazha_<?=$x?>_<?=$sd?>">
            <div class="row">
                <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                                <div class="form__label-wrap">
                                <label class="form__label">
                                        Как вы считаете объект продан Вами не ниже рыночной цены?
                                </label>
                                </div>
                                <select name="below_market_price[<?=$sd?>][<?=$x?>]" class="js-select">
                                        <option value="0" selected hidden>возможно ниже</option>
                                        <option value="1">нет не ниже</option>
                                </select>
                        </div>
                </div>
                <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                                <div class="form__label-wrap">
                                        <label class="form__label">
                                                Кем являлся покупатель?
                                        </label>
                                </div>
                                <select name="buyer_relative[<?=$sd?>][<?=$x?>]" class="js-select">
                                        <option value="0" selected hidden>Не родственником</option>
                                        <option value="1">Родственником</option>
                                </select>
                        </div>
                </div>
                <div class="form__col col-xs-12 col-sm-6 col-lg-4">
                        <div class="form__group">
                                <div class="form__label-wrap">
                                        <label class="form__label">
                                                Были ли у Вас долги на момент сделки?
                                        </label>
                                </div>
                                <select name="debts_time_transaction[<?=$sd?>][<?=$x?>]" class="js-select">
                                        <option value="0" selected hidden>Нет</option>
                                        <option value="1">Да</option>
                                </select>
                        </div>
                </div>
            </div>
        </div>
        <div class="sdelki prodazha_toggle_<?=$x?>_<?=$sd?> invise col-xs-8 col-sm-8 col-lg-8 container pb0 pt0"  id="dar_<?=$x?>_<?=$sd?>">
            <div class="row">
                <div class="form__col col-xs-12 col-sm-6 col-lg-6">
                        <div class="form__group">
                                <div class="form__label-wrap">
                                <label class="form__label">
                                        Кем являлся одаряемый
                                </label>
                                </div>
                                <select name="gift_relative[<?=$sd?>][<?=$x?>]" class="js-select">
                                        <option value="0" selected hidden>Не родственником</option>
                                        <option value="1">Родственником</option>
                                </select>
                        </div>
                </div>
                <div class="form__col col-xs-12 col-sm-6 col-lg-6">
                        <div class="form__group">
                                <div class="form__label-wrap">
                                <label class="form__label">
                                        Были ли у Вас долги на момент сделки?
                                </label>
                                </div>
                                <select name="debt_moment_donation[<?=$sd?>][<?=$x?>]" class="js-select">
                                        <option value="0" selected hidden>Нет</option>
                                        <option value="1">Да</option>
                                </select>
                        </div>
                </div>
            </div>
        </div>
        <?if (FALSE):?>
            <div class="form__col col-xs-12 col-sm-6 col-lg-3">
                <div class="form__group">
                        <div class="form__label-wrap">
                            <label class="form__label">
                                    Количество сделок с движимым имуществом
                            </label>
                        </div>
                        <select name="amount_transactions_movable_property[<?=$sd?>][<?=$x?>]" class="js-select">
                                <option selected hidden>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>					
                        </select>
                </div>
            </div>
        <? endif;?>
        <?if (FALSE):?>
            <div class="form__col col-xs-12 col-sm-6 col-lg-3">
                <div class="form__group">
                    <div class="form__label-wrap">
                    <label class="form__label">
                            Количество сделок с недвижимым имуществом
                    </label>
                    </div>
                    <select name="amount_transactions_immovable_property[<?=$sd?>][<?=$x?>]" class="js-select">
                            <option selected hidden>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>	
                    </select>
                </div>
            </div>
        <? endif;?>
    </div>
<?endfor;?>