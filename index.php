<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("description", "Услуги по банкротству граждан в юридической компании «ЛЕКС» в Волгограде. Узнать подробную информацию и задать вопросы можно на нашем сайте или по телефону +7 (8442) 23-53-23.");
$APPLICATION->SetPageProperty("title", "Банкротство граждан в Волгограде, помощь в банкротстве физического лица");
?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "slider_index",
        Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array("",""),
            "FILTER_NAME" => "",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "10",
            "IBLOCK_TYPE" => "sliders",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            "INCLUDE_SUBSECTIONS" => "Y",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "20",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array("LINK","BUTTON",""),
            "SET_BROWSER_TITLE" => "Y",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "Y",
            "SHOW_404" => "N",
            "SORT_BY1" => "SORT",
            "SORT_BY2" => "ID",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "DESC",
            "STRICT_SECTION_CHECK" => "N"
        )
    );?>

    <section class="section section_border_bottom about" id="next">
        <div class="about__container container">
            <div class="about__img-placeholder">
                <img src="<?=SITE_TEMPLATE_PATH?>/i/lifebuoy.jpg" srcset="<?=SITE_TEMPLATE_PATH?>/i/lifebuoy@2x.jpg 2x" class="about__img">
            </div>
            <div class="about__content">
                <h2>ЧТО ТАКОЕ БАНКРОТСТВО ФИЗИЧЕСКОГО ЛИЦА</h2>
                <div class="about__text text">
                    <p>Процедура банкротства физического лица предусмотрена 127-ФЗ «О несостоятельности (банкротстве)». Закон предусматривает полное списание долгов при процедуре «реализация имущества» в случае признания несостоятельности граждан, а также отсрочку, рассрочку исполнения или частичное прощение долга при заключении мирового соглашения между должником и кредитором. Признание гражданина банкротом необходимо тем, кто не в состоянии выполнить свои долговые обязательства по объективным причинам.</p>
                    <p>Юридическая компания «Лекс» оказывает помощь в банкротстве физического лица с момента появления этой процедуры в законодательстве Российской Федерации, поэтому наши клиенты выходят из ситуации без потерь. Мы работаем в городах Волгоград и Камышин, а также дистанционно – с выездом специалиста в город Саратов, другие населенные пункты Волгоградской и Саратовской областей.</p>
                    <p>Что получает должник, прошедший через банкротство при нашей помощи:</p>

                    <ul class="list-styled">
                        <li>100-процентное списание задолженности перед кредиторами, государством, муниципальными служащими;</li>
                        <li>отсутствие ограничений по выезду за границу;</li>
                        <li>полную свободу во владении, пользовании и распоряжении своим имуществом;</li>
                        <li>защиту от коллекторов, приставов, банков, настойчиво добивающихся выплат от клиентов и их семей;</li>
                        <li>отсутствие груза ответственности, связанного с наличием непогашенной задолженности, который мешает двигаться дальше.</li>
                    </ul>
                </div>
                <div class="about__action">
                    <a href="/bankrotstvo-grazhdan/" class="about__button button button_primary button_md">Узнать больше</a>
                </div>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section section_border_bottom types">
        <div class="types__container container container_sm">
            <h2 class="section__title">Критерии необходимые для признания должника банкротом</h2>
            <div class="types__list row">
                <div class="types__item col-xs-12 col-sm-3">
                    <i class="types__icon icon-money"></i>
                    <h5 class="types__title">Наличие задолженности</h5>
                </div>
                <div class="types__item col-xs-12 col-sm-3">
                    <i class="types__icon icon-graph"></i>
                    <h5 class="types__title">Отсутствие достаточного дохода</h5>
                </div>
                <div class="types__item col-xs-12 col-sm-3">
                    <i class="types__icon icon-man-with-money"></i>
                    <h5 class="types__title">Отсутствие возможности оплатить задолженность</h5>
                </div>
                <div class="types__item col-xs-12 col-sm-3">
                    <i class="types__icon icon-statement"></i>
                    <h5 class="types__title">Отсутствие имущества для погашенения задолженности</h5>
                </div>
            </div>
            <div class="types__text text">
                Физ. лицо может иметь или не иметь работу, иметь или не иметь имущество – но всегда существуют пути для реализации процедуры при всех указанных условиях.
            </div>
            <div class="types__action">
                <a href="/kompaniya/stati/kto-mozhet-byt-priznan-bankrotom/" class="types__button button button_primary button_md">Узнать больше</a>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section workflow">
        <div class="workflow__container container">
            <h2 class="section__title">ПРОЦЕСС РАБОТЫ</h2>
            <p class="section__subtext">Как строится процесс работы с клиентом по признанию его банкротом</p>
            <ol class="workflow__list">
                <li class="workflow__item">
                    <i class="workflow__icon icon-question"></i>
                    <h6 class="workflow__title">Обращение за юридической помощью в офисы компании</h6>
                    <div class="workflow__text">в Волгограде или Камышине<div>
                </li>
                <li class="workflow__item">
                    <i class="workflow__icon icon-bubles"></i>
                    <h6 class="workflow__title">Бесплатная консультация</h6>
                    <div class="workflow__text">Расчет стоимости процедуры банкротства<div>
                </li>
                <li class="workflow__item">
                    <i class="workflow__icon icon-contract"></i>
                    <h6 class="workflow__title">Заключение договора</h6>
                    <div class="workflow__text">Внесение клиентом аванса по договору<div>
                </li>
                <li class="workflow__item">
                    <i class="workflow__icon icon-shield"></i>
                    <h6 class="workflow__title">Сбор документов</h6>
                    <div class="workflow__text">При необходимости оказываем помощь в сборе документов<div>
                </li>
                <li class="workflow__item workflow__item_primary">
                    <i class="workflow__icon icon-docs"></i>
                    <h6 class="workflow__title">Подготовка  заявления</h6>
                    <div class="workflow__text">Подготовка и подача заявления и документов в арбитражный суд<div>
                </li>
                <li class="workflow__item workflow__item_primary">
                    <i class="workflow__icon icon-lawyer"></i>
                    <h6 class="workflow__title">Сопровождение процедуры банкротства </h6>
                    <div class="workflow__text">Представление интересов в суде, работа с финансовым управляющим, участие в собраниях кредиторов и пр.<div>
                </li>
                <li class="workflow__item workflow__item_primary">
                    <i class="workflow__icon icon-handshake"></i>
                    <h6 class="workflow__title">Получение судебного акта о списании долгов клиента</h6>
                </li>
            </ol>
        </div><!-- /.container -->
    </section>

    <?php require($_SERVER['DOCUMENT_ROOT'].'/include/calcul2.php'); ?>
    
    <?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback", 
	"bankrot_feedback", 
	array(
		"COMPONENT_TEMPLATE" => "bankrot_feedback",
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => "work4egor@gmail.com",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "MESSAGE",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "7",
		)
	),
	false
);?>

    <section class="section section_border_bottom features">
        <div class="features__container container container_md">
            <h2 class="section__title">ПОЧЕМУ ВАМ ВАЖНО РАБОТАТЬ ИМЕННО С НАМИ</h2>
            <div class="features__list">
                <div class="features__item">
                    <div class="features__item-img-placeholder">
                        <img src="<?=SITE_TEMPLATE_PATH?>/i/counting-money.jpg" srcset="<?=SITE_TEMPLATE_PATH?>/i/counting-money@2x.jpg 2x" class="features__item-img">
                    </div>
                    <div class="features__item-content">
                        <h3 class="features__item-title">Самые доступные цены по рынку</h3>
                        <div class="features__item-text text">
                            <ul>
                               <li>Недорогие услуги юриста. Цены – ниже средних, взимаемых за аналогичные услуги.</li>
                               <li>Отсутствие сложной системы тарифов.</li>
                               <li>Даем прогноз по решению суда, рассчитываем сразу итоговую стоимость процедуры признания несостоятельности гражданина.</li>
                               <li>Фиксируем суммы на старте работы – гражданин не доплачивает в случае непредвиденных расходов не по его вине.</li>
                               <li>Предоставляем рассрочку до 14 месяцев.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="features__item">
                    <div class="features__item-img-placeholder">
                        <img src="<?=SITE_TEMPLATE_PATH?>/i/discussion.jpg" srcset="<?=SITE_TEMPLATE_PATH?>/i/discussion@2x.jpg 2x" class="features__item-img">
                    </div>
                    <div class="features__item-content">
                        <h3 class="features__item-title">ОКАЗАНИЕ УСЛУГИ «ПОД КЛЮЧ»</h3>
                        <div class="features__item-text text">
                            <ul>
                                <li>Даем гарантию 100% результата. В противном случае — возвращаем оплаченные деньги.</li>
                                <li>Ведем клиента от стадии предоставления консультации до момента получения судебного акта о признании клиента банкротом.</li>
                                <li>Помогаем со сбором документов.</li>
                                <li>Участвуем в судебных заседаниях, на собраниях кредиторов вместо клиента.</li>
                                <li>Контролируем работу арбитражного управляющего и взаимодействуем с ним.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="features__item">
                    <div class="features__item-img-placeholder">
                        <img src="<?=SITE_TEMPLATE_PATH?>/i/files.jpg" srcset="<?=SITE_TEMPLATE_PATH?>/i/files@2x.jpg 2x" class="features__item-img">
                    </div>
                    <div class="features__item-content">
                        <h3 class="features__item-title">Банкротство от «А» до «Я»</h3>
                        <div class="features__item-text text">
                           <p>Проводим работу по юридической «чистоте» клиента на стадии до подачи заявления о несостоятельности, чтобы процедура прошла по запланированному сценарию.</p>
                           <p>Получаем решение суда о завершении процедуры и списания задолженности.</p>
                           <p>Связываемся с кредиторами и «банками», уведомляем последних о признании гражданина банкротом.</p>
                           <p>В случае предъявления требований от кредиторов - помогаем с юридической защитой клиента (в том числе и в суде общей юрисдикции).</p>
                        </div>
                    </div>
                </div>
                <div class="features__item">
                    <div class="features__item-img-placeholder">
                        <img src="<?=SITE_TEMPLATE_PATH?>/i/handshake.jpg" srcset="<?=SITE_TEMPLATE_PATH?>/i/handshake@2x.jpg 2x" class="features__item-img">
                    </div>
                    <div class="features__item-content">
                        <h3 class="features__item-title">Богатый опыт работы в сфере банкротства</h3>
                        <div class="features__item-text text">
                            <p>Работаем по банкротству физических лиц с момента вступления в силу соответствующего Закона.</p>
                            <p>Большой опыт работы в области банкротства граждан имеют специалисты нашей компании, арбитражные управляющие, с которыми мы сотрудничаем.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section section_border_bottom cases">
        <div class="cases__container container">
            <h2 class="section__title">УСПЕШНЫЕ ДЕЛА</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"dela.list", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "successful_cases",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "arrows_adm",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "fio",
			1 => "gorod",
			2 => "dolg",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "TIMESTAMP_X",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "dela.list"
	),
	false
);?>
            <div class="cases__action">
                <a href="uspeshnye-dela/" class="cases__button button button_primary button_lg">Посмотреть все дела</a>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section faq">
        <div class="faq__container container container_sm">
            <h2 class="section__title">ВОПРОС-ОТВЕТ</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"faq_list", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "faq",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "TIMESTAMP_X",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "faq_list"
	),
	false
);?>
            <div class="faq__action">
                <a href="vopros-otvet/" class="faq__button button button_primary button_lg">Все вопросы и ответы</a>
            </div>
        </div><!-- /.container -->
    </section>

    <div class="contacts">
        <div class="contacts__map" id="map"></div>
    </div>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>