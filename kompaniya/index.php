<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания \"Лекс\" занимается оказанием профессиональных юридических услуг в сфере банкротства граждан, в том числе граждан - индивидуальных предпринимателей.");
$APPLICATION->SetPageProperty("title", "О компании | Юридическая фирма Лекс");
$APPLICATION->SetTitle("Компания");
?>
    <section class="section section_border_bottom_light">
        <div class="container container_xs">
            <h1 class="section__title">О Компании</h1>
            <div class="text">
                <p>ООО «Юридическая Фирма «Лекс» основана в 2009 году. С 2015 года основным направлением деятельности компании является оказание профессиональных юридических услуг в сфере банкротства граждан, в том числе  граждан - индивидуальных предпринимателей.</p>
                <p>Принципы, на которых основывается деятельность нашей компании – профессионализм, ответственности перед Клиентом,  открытость, высокое качество оказания услуг. Наше кредо – помочь людям начать Новую жизнь – без долгов!</p>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section section_border_bottom_light features features_sm">
        <div class="features__container container container_xs">
            <div class="features__list">
                <div class="features__item">
                    <div class="features__item-img-placeholder">
                        <img src="/i/ceo.jpg" srcset="/i/ceo@2x.jpg 2x" class="features__item-img">
                    </div>
                    <div class="features__item-content">
                        <h2 class="features__item-title">Руководство</h2>
                        <div class="features__item-text text">
                            <p>Руководство фирмой осуществляет директор ООО «ЮФ «Лекс» Аралов Роман Николаевич. Юридический стаж – с 2006 года. Образование высшее. В 2006 году окончил с Саратовскую Государственную Академию Права с «красным дипломом». Все сотрудники компании - профессиональные юристы с большим опытом работы, которые  всегда готовые к решению любых задач.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="section section_border_bottom_light stats">
        <div class="stats__container container container_xs">
            <p>Наша компания постоянно развивается. В настоящее время мы имеем  два представительства – в г. Волгограде и в г. Камышин. Так же осуществляем дистанционное оказание услуг (с выездом специалиста) на территории других районов Волгоградской области, г. Саратов  и Саратовской области.</p> 
        </div><!-- /.container -->
    </section>
        <section class="section contacts">

        <div class="contacts__map mb40" id="map"></div>
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "office", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                "AJAX_MODE" => "N", // Включить режим AJAX
                "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
                "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",  // Учитывать права доступа
                "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                "CACHE_TYPE" => "A",    // Тип кеширования
                "CHECK_DATES" => "Y",   // Показывать только активные на данный момент элементы
                "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
                "DISPLAY_DATE" => "N",  // Выводить дату элемента
                "DISPLAY_NAME" => "Y",  // Выводить название элемента
                "DISPLAY_PICTURE" => "N",   // Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",  // Выводить текст анонса
                "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                "FIELD_CODE" => array(  // Поля
                    0 => "NAME",
                    1 => "DETAIL_TEXT",
                    2 => "",
                ),
                "FILTER_NAME" => "",    // Фильтр
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
                "IBLOCK_ID" => "7", // Код информационного блока
                "IBLOCK_TYPE" => "offices", // Тип информационного блока (используется только для проверки)
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
                "INCLUDE_SUBSECTIONS" => "N",   // Показывать элементы подразделов раздела
                "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                "NEWS_COUNT" => "4",    // Количество новостей на странице
                "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
                "PAGER_TITLE" => "Новости", // Название категорий
                "PARENT_SECTION" => "", // ID раздела
                "PARENT_SECTION_CODE" => "",    // Код раздела
                "PREVIEW_TRUNCATE_LEN" => "",   // Максимальная длина анонса для вывода (только для типа текст)
                "PROPERTY_CODE" => array(   // Свойства
                    0 => "coordinates",
                    1 => "",
                ),
                "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
                "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
                "SET_META_DESCRIPTION" => "N",  // Устанавливать описание страницы
                "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
                "SET_STATUS_404" => "N",    // Устанавливать статус 404
                "SET_TITLE" => "N", // Устанавливать заголовок страницы
                "SHOW_404" => "N",  // Показ специальной страницы
                "SORT_BY1" => "SORT",   // Поле для первой сортировки новостей
                "SORT_BY2" => "TIMESTAMP_X",    // Поле для второй сортировки новостей
                "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
                "STRICT_SECTION_CHECK" => "N",  // Строгая проверка раздела для показа списка
            ),
            false
        );?>

    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>