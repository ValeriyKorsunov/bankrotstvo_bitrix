<?php

//include 'dBug.php';
//include 'Dumphper.php';

/**
 * Функция выводит массив на экран
 * param  $arr  Array Массив данных
 * param  $var_dump Bool Если true, то выводит в массиве и типы данных
 * Void
 */

include 'PHPDebug.php';

function vardump($arr, $var_dump = true)
{
//    echo "<pre style='background: #222;color: #54ff00;padding: 20px;'>";
    echo "<pre>";
    if ($var_dump){
        var_dump($arr);
    }
    else{
        print_r($arr);
    }
    echo "</pre>";
}
