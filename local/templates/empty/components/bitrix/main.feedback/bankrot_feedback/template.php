<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$phone_err = '';
$mail_adr_err = '';
?>

<?
//echo '</pre>';
//var_dump($arResult);
//echo '</pre>';
?>

<section class="section section_bg_black feedback" id="feedback">
    <div class="container container_md">
        <?
        if(!empty($arResult["ERROR_MESSAGE"])){
	        //foreach($arResult["ERROR_MESSAGE"] as $v) ShowError($v);
            if($arResult["MESSAGE"]=='')  $phone_err = 'has-error';
            if($arResult["AUTHOR_NAME"]=='')  $mail_adr_err = 'has-error';
        }
        if(strlen($arResult["OK_MESSAGE"]) > 0)
        {
        ?>
            <h2 class="m0 mt10"><?=$arResult["OK_MESSAGE"]?></h2>
        <?
        } else {
        ?>
        <?if ($_SERVER["SCRIPT_NAME"] == "/bankrotstvo-grazhdan/bankrotstvo-rukovoditeley/index.php"): ?>
            <h2 class="section__title">У Вас остались вопросы?</h2>
            <p class="section__subtext">Вам интересно узнать о процедуре банкротства руководителя (учредителя) организации более подробно? Позвоните нам и мы запишем Вас на бесплатную консультацию.</p>
        <? else: ?>
            <h2 class="section__title">Остались вопросы? </h2>
            <p class="section__subtext">Заполните форму для того, чтобы наши сотрудники связались с вами.</p>
        <? endif ?>
        <form method="post" class="feedback__form form"  id="contact_form">
            <?=bitrix_sessid_post()?>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form__group <?=$mail_adr_err?>">
                        <input required type="text" name="user_name" class="form__control" placeholder="Ваше имя*" tabindex="10" requered>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form__group <?=$phone_err?>">
                        <input required type="text" name="MESSAGE" class="form__control js-tel" placeholder="Телефон*" tabindex="20" requered>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form__group">
                        <input required type="text" class="form__control" placeholder="E-mail" tabindex="30" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
                    </div>
                </div>
            </div>
            <div class="form__group form__group_center">
                <em>Поля отмеченые (*), обязательны для заполнения.</em><br>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control__input checked" tabindex="40" name="agreement" value="1" required checked="checked">
                    <div class="custom-control__indicator"></div>
                    <em>Отправляя данную форму, вы даете согласие на <a href="/politika-konfidentsialnosti/" target="_blank" class="link link_white link_underlined">обработку своих персональных данных</a></em>
                </label>
            </div>
            <div class="form__group form__group_submit">
                <input type="submit" name="submit" class="form__button button button_info button_lg" value="Отправить" tabindex="50">
            </div>
        </form>
        <?
        } 
        ?>
    </div><!-- /.container -->
</section>
