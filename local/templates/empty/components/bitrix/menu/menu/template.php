<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//echo '<pre>';
//var_dump($arResult);
//echo '</pre>';
?>

<?if (!empty($arResult)):?>
    <nav class="nav">
        <div class="nav__container container">
            <button class="nav__toggle icon-menu js-nav-toggle"></button>
            <ul class="nav__list js-nav">

                <?
                $previousLevel = 0;
                foreach($arResult as $arItem):?>

                    <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                        <?=str_repeat("</ul></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                    <?endif?>

                    <?if ($arItem["IS_PARENT"]):?>

                        <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                            <li class="nav__item">
                                <a href="<?=$arItem["LINK"]?>" class="nav__link <?if($arItem["SELECTED"]):?>active<?endif?>">
                                    <?=$arItem["TEXT"]?>
                                </a>
                                <div class="nav__subnav subnav">
                                    <ul class="subnav__list">
                        <?else:?>
                            <li class="subnav__item <?if($arItem["SELECTED"]):?>active<?endif?>">
                                <a href="<?=$arItem["LINK"]?>" class="subnav__link link link_black">
                                    <?=$arItem["TEXT"]?>
                                </a>
                                <div class="nav__subnav subnav">
                                    <ul class="subnav__list">
                        <?endif?>

                    <?else:?>

                        <?if ($arItem["PERMISSION"] > "D"):?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li class="nav__item">
                                    <a href="<?=$arItem["LINK"]?>" class="nav__link <?if($arItem["SELECTED"]):?>active<?endif?>">
                                        <?=$arItem["TEXT"]?>
                                    </a>
                                </li>
                            <?else:?>
                                <li class="subnav__item <?if($arItem["SELECTED"]):?>active<?endif?>">
                                    <a href="<?=$arItem["LINK"]?>" class="subnav__link link link_black">
                                        <?=$arItem["TEXT"]?>
                                    </a>
                                </li>
                            <?endif?>

                        <?else:?>

                            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li class="nav__item">
                                    <a href="<?=$arItem["LINK"]?>" class="nav__link <?if($arItem["SELECTED"]):?>active<?endif?>">
                                        <?=$arItem["TEXT"]?>
                                    </a>
                                </li>
                            <?else:?>
                                <li class="subnav__item <?if($arItem["SELECTED"]):?>active<?endif?>">
                                    <a href="<?=$arItem["LINK"]?>" class="subnav__link link link_black">
                                        <?=$arItem["TEXT"]?>
                                    </a>
                                </li>
                            <?endif?>

                        <?endif?>

                    <?endif?>

                    <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                <?endforeach?>

                <?if ($previousLevel > 1)://close last item tags?>
                    <?=str_repeat("</li>", ($previousLevel-1) );?>
                <?endif?>
            </ul>
        </div>
    </nav>
<?endif?>