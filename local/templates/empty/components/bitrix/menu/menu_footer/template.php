<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
$new_arResult = array();
$y=0;
for ($i = 0; $i < count($arResult); $i++){
    if ($i==0){ $new_arResult[$y]=$arResult[$i]; continue;}
    if( $arResult[$i]['DEPTH_LEVEL']==1)$y++;
    if($arResult[$i]['DEPTH_LEVEL'] == 1)$new_arResult[$y]=$arResult[$i];
    if($arResult[$i]['DEPTH_LEVEL']>1)$new_arResult[$y]['LEVEL'][]=$arResult[$i];
}?>

    <div class="footer__left col-xs-12 col-lg-8">
        <div class="footer__nav">
            <?foreach($new_arResult as $arItem):?>
                <?if($arItem['IS_PARENT']):?>
                    <div class="footer__nav-section">
                        <div class="footer__nav-title h6" style="text-transform: uppercase;">
                            <a href="<?=$arItem['LINK']?>" class="link link_black">
                                <?=$arItem['TEXT']?>
                            </a></div>
                        <ul class="footer__nav-list">
                            <?foreach ($arItem['LEVEL'] as $level_item):?>
                                <li class="footer__nav-item"><a href="<?=$level_item['LINK']?>" class="link link_black"><?=$level_item['TEXT']?></a></li>
                            <?endforeach;?>
                        </ul>
                    </div>
                <?endif;?>
            <?endforeach?>
            <div class="footer__nav-section">
                <?foreach($new_arResult as $arItem):?>
                    <?if(!$arItem['IS_PARENT']):?>
                        <div style="text-transform: uppercase;" class="footer__nav-title h6"><a href="<?=$arItem['LINK']?>" class="link link_black"><?=$arItem['TEXT']?></a></div>
                    <?endif;?>
                <?endforeach?>
            </div>
        </div>
    </div>

<?endif?>

