<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
//vardump($arResult['ITEMS']);
?>

<section class="section">
    <h2 class="section__title">Документы</h2>
    <div class="docs docs_stacked">
        <div class="docs__list">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>

                <?
                $db_props = CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array(), Array());
                if($ar_props = $db_props->Fetch()){
                    //vardump($ar_props["FILE_TYPE"]);
                    $rsFile = CFile::GetByID($ar_props['VALUE']);
                    //vardump($rsFile);
                    CFile::GetPath($ar_props['VALUE']);
                    $arFile = $rsFile->Fetch();
                    $kb = round($arFile['FILE_SIZE'] / 1024);
                    //vardump($arFile);
                ?>
                <a href="<?echo CFile::GetPath($ar_props['VALUE'])?>" class="docs__item">
                    <i class="docs__icon icon-doc"></i>
                    <span class="docs__link"><?echo $arItem["NAME"]?></span>
                    <span class="docs__format">(<?echo $ar_props["FILE_TYPE"]?>, <?echo $kb?>Kb)</span>
                </a>
                <?}?>
            <?endforeach;?>
        </div>
    </div>
</section>

