<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="cases__list">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="cases__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a class="cases__item-img-placeholder" target="_blank" href='<?=CFile::GetPath($arItem["PROPERTIES"]["PDF"]["VALUE"])?>'><img
                        class="cases__item-img"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						/></a>
			<?else:?>
				<img
                    class="cases__item-img"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					/>
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
            <h3 class="cases__item-title"> <?echo $arItem["NAME"]?></h3>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
        <div class="cases__item-text text">
            <p><?=$arItem["DISPLAY_PROPERTIES"]['fio']['DISPLAY_VALUE']?><br>г. <?=$arItem["DISPLAY_PROPERTIES"]['gorod']['DISPLAY_VALUE']?></p>
            <p>Списано долга<br>
            <?=$arItem["DISPLAY_PROPERTIES"]['dolg']['DISPLAY_VALUE']?> ₽</p>
        </div>
        <?// echo '<code>'; var_dump($arItem["DISPLAY_PROPERTIES"] ); echo '</code>';?>
        <?if($arItem["DETAIL_PICTURE"]):?>
        <div class="cases__item-action">
            <a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" rel="review" class="cases__item-button button button_dashed button_sm">Читать отзыв клиента</a>
        </div>
        <?endif;?>
    </div>
<?endforeach;?>
</div>

<?$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name);?>
<?if($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer<$arResult["NAV_RESULT"]->nEndPage):?>
    <div class="cases__action" id="btn_<?=$bxajaxid?>">
        <a data-ajax-id="<?=$bxajaxid?>"
           href="javascript:void(0)"
           data-show-more="<?=$arResult["NAV_RESULT"]->NavNum?>"
           data-next-page="<?=($arResult["NAV_RESULT"]->NavPageNomer + 1)?>"
           data-max-page="<?=$arResult["NAV_RESULT"]->nEndPage?>"
           class="cases__button button button_primary button_xl">Показать еще</a>
    </div>
<?endif;?>
