<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?/*
<div class="contacts__container container container_md">
	<div class="contacts__list row">
		<div class="contacts__left col-xs-12 col-lg-4">
			<h4 class="contacts__title">Центральный офис</h4>

			<div class="contacts__item">
			<?=$arResult["ITEMS"][0]['PREVIEW_TEXT']?>
			</div>
		</div>
		<div class="contacts__right col-xs-12 col-lg-8">
			<h4 class="contacts__title">Другие офисы</h4>
			<div class="row">
				<? foreach($arResult["ITEMS"] as $key=>$arItem):?>
					<?if($key != 0):?>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="contacts__item">
								<?=$arItem["PREVIEW_TEXT"]?>
							</div>
						</div>
					<?endif;?>
				<?endforeach;?>
			</div>
		</div>
	</div>
</div><!-- /.container --> 
*/?>

<div class="contacts__container container container_md">
	<div class="contacts__list row">
		<div class="col-xs-12 col-lg-12">
			<h4 class="contacts__title">Наши офисы</h4>
			<div class="row">
				<? foreach($arResult["ITEMS"] as $key=>$arItem):?>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="contacts__item">
							<?=$arItem["PREVIEW_TEXT"]?>
						</div>
					</div>
				<?endforeach;?>
			</div>
		</div>
	</div>
</div><!-- /.container --> 