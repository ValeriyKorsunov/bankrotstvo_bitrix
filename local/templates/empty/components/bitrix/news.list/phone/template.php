<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <li role="presentation">
        <a role="menuitem" tabindex="-1" href="/<?echo $arItem["CODE"]?>/" data-phone="<?echo $arItem["PROPERTIES"]["PHONE"]["VALUE"]?>" data-phoneurl="<?= str_replace(array('(',')','-',' '),'', $arItem['PROPERTIES']['PHONE']['VALUE'])?>" data-email="<?echo $arItem["PROPERTIES"]["MAIL"]["VALUE"]?>"><?echo $arItem["NAME"]?></a>
    </li>
<?endforeach;?>