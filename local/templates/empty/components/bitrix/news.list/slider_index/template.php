<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="section section_bg_black hero">
    <div class="hero__img js-hero-img"></div>
    <div class="container container_sm">
        <div class="hero__slider js-hero-slider">
			<?foreach($arResult["ITEMS"] as $arItem):?>
	            <div class="hero__item" data-bg="background-image:url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"]?>);">
	                <h1 class="hero__title"><?echo $arItem["NAME"]?></h1>
	                <div class="hero__text text text_lg">
	                    <p><?echo $arItem["PREVIEW_TEXT"]?></p>
	                </div>
	                <div class="hero__action">
	                    	<? if ($arItem["PROPERTIES"]["BUTTON"]["VALUE"]):?>
			                    <a href="<?echo $arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="button button_info" title="">
	    	                		<?echo $arItem["PROPERTIES"]["BUTTON"]["VALUE"]?>
								</a>
	                    	<? else: ?>
	                    		<a href="<?echo $arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="button button_info button_round" title="">
	                    			<i class="button__icon icon-arrow"></i>
	                    		</a>
	                    	<? endif ?>
	                </div>
	            </div>
			<?endforeach;?>
    	</div>
	</div>
</section>