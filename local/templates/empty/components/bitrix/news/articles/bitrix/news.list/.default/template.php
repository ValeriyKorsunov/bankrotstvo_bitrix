<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section articles">
    <div class="container container_md">
    	<h1 class="section__title">Статьи</h1>
		<div class="articles__list">
		    <?foreach($arResult["ITEMS"] as $arItem):?>
		        <?
		        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		        ?>
		        <div class="articles__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
		                <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
		                    <a class="articles__item-img-placeholder" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
		                        <img
		                            class="articles__item-img"
		                            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
		                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
		                            />
		                    </a>
		                <?else:?>
		                    <img class="articles__item-img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
		                <?endif;?>
		            <?endif?>
		            <div class="articles__item-content">
		                <?if($arParams["DISPLAY_DATE"]!="N"):?>
		                    <div class="articles__item-date"><?echo $arItem ["DATE_CREATE"]?></div>
		                <?endif?>

		                <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		                <h3 class="articles__item-title">
		                    <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
		                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
		                            <?echo $arItem["NAME"]?>
		                        </a>
		                    <?else:?>
		                        <?echo $arItem["NAME"]?>
		                    <?endif;?>
		                </h3>
		                <?endif;?>

		                <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		                <div class="articles__item-text">
		                    <?echo $arItem["PREVIEW_TEXT"];?>
		                </div>
		                <?endif;?>

		                <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
		                    <div style="clear:both"></div>
		                <?endif?>
		                <div class="articles__item-action">
		                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Читать полностью</a>
		                </div>
		            </div>

		            <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		                <small>
		                <?=$arProperty["NAME"]?>:&nbsp;
		                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
		                    <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		                <?else:?>
		                    <?=$arProperty["DISPLAY_VALUE"];?>
		                <?endif?>
		                </small><br />
		            <?endforeach;?>
		        </div>
		    <?endforeach;?>
		</div>

		<?$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name);?>

		<?if($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage):?>
		    <div class="articles__action" id="btn_<?=$bxajaxid?>">
		        <a data-ajax-id="<?=$bxajaxid?>"
		           href="javascript:void(0)"
		           data-show-more="<?=$arResult["NAV_RESULT"]->NavNum?>"
		           data-next-page="<?=($arResult["NAV_RESULT"]->NavPageNomer + 1)?>"
		           data-max-page="<?=$arResult["NAV_RESULT"]->nEndPage?>"
		           class="cases__button button button_primary button_xl">Показать еще</a>
		    </div>
		<?endif;?>
	</div>
</section>