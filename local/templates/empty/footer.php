<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

    <?if ($_SERVER["SCRIPT_NAME"] != "/index.php"): ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.feedback",
            "bankrot_feedback",
            array(
                "COMPONENT_TEMPLATE" => "bankrot_feedback",
                "USE_CAPTCHA" => "N",
                "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                "EMAIL_TO" => "e.sharlay@pixelplus.ru",
                "REQUIRED_FIELDS" => array(
                    0 => "NAME",
                    1 => "MESSAGE",
                ),
                "EVENT_MESSAGE_ID" => array(
                    0 => "7",
                )
            ),
            false
        );?>
    <?endif;?>
    </main><!-- /.content -->
</div>

<footer class="footer">
    <div class="footer__top">
        <div class="footer__top-container container">
            <div class="row">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_footer", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                        1 => "",
                    ),
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                    false
                );?>

                <div class="footer__right col-xs-12 col-lg-4">
                    <div class="footer__contacts">
                        <div clas="footer__contacts-info">
                            <?$APPLICATION->IncludeFile(SITE_DIR."/include/phone.php", array(), array(MODE => "html")); ?>
                            <?$APPLICATION->IncludeFile(SITE_DIR."/include/mail.php", array(), array(MODE => "html")); ?>
                        </div>
                        <?if ($_SERVER["SCRIPT_NAME"] != "/index.php"): ?>
                            <a href="/tseny/#calculator" class="footer__contacts-button button button_primary_outline button_lg">Рассчитать стоимость банкротства</a>
                        <? else: ?> 
                            <a href="#calculator" class="footer__contacts-button button button_primary_outline button_lg">Рассчитать стоимость банкротства</a>
                        <? endif ?>
                        <a href="#feedback" class="footer__contacts-feedback link link_black link_with_icon link_feedback"><i class="link__icon icon-tel"></i>Обратная связь</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </div>
    <div class="footer__bottom">
        <div class="footer__bottom-container container">
            <div class="row">
                <div class="footer__left col-xs-12 col-sm-8">
                    &copy; <?php echo date('Y') ?> Юридическая Фирма &laquo;ЛЕКС&raquo;<br>
                    <a href="/politika-konfidentsialnosti/" target="_blank" class="footer__link">Политика конфиденциальности</a>
                </div>
                <div class="footer__right col-xs-12 col-sm-4">
                    <?if(!$isIndexPage):?>
                        <a href="http://www.pixelstar.ru" target="_pixelstar">Разработка сайта</a><br>
                        Пиксель Стар
                    <? else:?>
                        Разработка сайта<br>
                        <a href="http://www.pixelstar.ru" target="_pixelstar">Пиксель Стар</a>
                    <? endif ?>
                </div>
            </div>
        </div><!-- /.container -->
    </div>
</footer><!-- /.footer -->

<a href="#top" id="totop" class="button button_totop button_round button_fixed button_rotate" title="" tabindex="0"><i class="button__icon icon-arrow"></i></a>

</body>
</html>