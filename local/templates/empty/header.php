<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
$isIndexPage = $APPLICATION->GetCurPage(true) == SITE_DIR."index.php";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/main.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/vendor/bootstrap/css/bootstrap.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/vendor/magnific-popup/magnific-popup.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/vendor/jquery-ui/jquery-ui.min.css");
    ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    Asset::getInstance()->addString('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/jquery.maskedinput.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/bootstrap/js/bootstrap.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/bootstrap-select/js/bootstrap-select.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/magnific-popup/jquery.magnific-popup.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/truncatable.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/jquery-ui/jquery-ui.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/calculator.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");
    Asset::getInstance()->addString('<script src="https://maps.googleapis.com/maps/api/js?v=3.20&sensor=false"></script>');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/map.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/ajax.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/calculate-ajax.js");
   //

    ?>
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<div id="top"></div>
<div class="page">
    <header class="header">
        <div class="header__container container">
            <div class="header__logo">
                <a href="/" class="header__logo-link">
                    <img src="<?=SITE_TEMPLATE_PATH?>/i/logo.svg" alt="ЛЕКС - Юридическая фирма" class="header__logo-img">
                </a>
            </div>
            <div class="header__contacts">
                <div class="header__contacts-links">
                    <a href="#feedback" class="header__contacts-feedback link link_black link_with_icon link_feedback"><i class="link__icon icon-tel"></i>Обратная связь</a>
                    <?if ($_SERVER["SCRIPT_NAME"] != "/index.php"): ?>
                        <a href="/tseny/#calculator" class="header__contacts-button button button_primary_outline button_lg">Рассчитать стоимость банкротства</a>
                    <? else: ?> 
                        <a href="#calculator" class="header__contacts-button button button_primary_outline button_lg">Рассчитать стоимость банкротства</a>
                    <? endif ?>
                </div>
                <div class="header__contacts-location">
                    <?$APPLICATION->IncludeFile(SITE_DIR."/include/phone.php", array(), array(MODE => "html")); ?>
                    <div class="phone_understroke">
                        (Бесплатно по России)
                    </div>
                    <div class="header__contacts-city">
                        Ваш город:
                        <div class="dropdown location">
                            <button class="dropdown-toggle js-location-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Волгоград
                            </button>
                            <ul class="dropdown-menu dropdown-menu__location" role="menu" aria-labelledby="dropdownMenu1">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:news.list",
                                        "phone",
                                        Array(
                                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_ADDITIONAL" => "",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "CACHE_FILTER" => "N",
                                            "CACHE_GROUPS" => "Y",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_TYPE" => "A",
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "DISPLAY_DATE" => "Y",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "Y",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "FIELD_CODE" => array("",""),
                                            "FILTER_NAME" => "",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "IBLOCK_ID" => "9",
                                            "IBLOCK_TYPE" => "region",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "INCLUDE_SUBSECTIONS" => "Y",
                                            "MESSAGE_404" => "",
                                            "NEWS_COUNT" => "120",
                                            "PAGER_BASE_LINK_ENABLE" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => ".default",
                                            "PAGER_TITLE" => "Новости",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "PREVIEW_TRUNCATE_LEN" => "",
                                            "PROPERTY_CODE" => array("MAIL","PHONE",""),
                                            "SET_BROWSER_TITLE" => "N",
                                            "SET_LAST_MODIFIED" => "N",
                                            "SET_META_DESCRIPTION" => "Y",
                                            "SET_META_KEYWORDS" => "Y",
                                            "SET_STATUS_404" => "N",
                                            "SET_TITLE" => "N",
                                            "SHOW_404" => "N",
                                            "SORT_BY1" => "SORT",
                                            "SORT_BY2" => "ID",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_ORDER2" => "DESC",
                                            "STRICT_SECTION_CHECK" => "N"
                                        )
                                    );?>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </header><!-- /.header -->

    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "menu",
        Array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "COMPONENT_TEMPLATE" => "menu",
            "DELAY" => "N",
            "MAX_LEVEL" => "2",
            "MENU_CACHE_GET_VARS" => array(),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "top",
            "USE_EXT" => "N"
        )
    );?><!-- /.nav -->

    <main class="content">
        <?if(!$isIndexPage):?>
            <?$APPLICATION->IncludeComponent(
            	"bitrix:breadcrumb", 
            	"breadcrumb", 
            	array(
            		"COMPONENT_TEMPLATE" => "breadcrumb",
            		"START_FROM" => "0",
            		"PATH" => "",
            		"SITE_ID" => "s1"
            	),
            	false
            );?>
        <?endif;?>
	
						