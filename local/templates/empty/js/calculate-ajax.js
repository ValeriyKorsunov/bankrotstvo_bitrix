function changeSdelki(object)
{
    if (object.val() != '0')
    {
        console.log(object.attr('name'));
        $.ajax
        ({
            url : '/include/calcul_sdelki.php',
            type : 'POST',
            data : { 'sdelki': object.val() , 'typeSd': object.data('deal') },
            success : function (data)
            {
                if (data)
                {
                //console.info(data);
                $('#'+object.attr('name')).html(data);
                $('.js-select').selectpicker({size: 'auto',}); // обновить верстку для селектов
                }
            }
        });
    } 
    else 
    {
        console.log('очистить');
        $("#"+object.attr('name')).html('');
    }
}

$(document).on('click', '#js-calculate-ajax', function(){
    var data_form = {};

    var data = $('.section.section_bg_gray.calculator').find('input, select').serializeArray();
    $.each(data, function() {
        //console.log(this.name + ' = ' + this.value);
        data_form[this.name] = this.value;
    });

    data_form['X'] = $('#summa').val();        // Сумма долга X
    //data_form['DMES'] = $('.js-range-amount-2').text();     // Ежемесячный доход должника: Dmes
    //data_form['ALIM'] = $('.js-range-amount-3').text();     // Размер выплаты по алиментам (если имеются)(руб.): Alim
    //data_form['STR'] = $('.js-range-amount-4').text();      // Общая стоимость транспорта: Str
    //data_form['STNZ'] = $('.js-range-amount-5').text();     // общая стоимость залогового недвижимого имущества (в том числе ипотека) (руб.) Stnz
    //data_form['STN'] = $('.js-range-amount-6').text();      // Общая стоимость недвижимого имущества (в собственности) Stn
    //data_form['STNS'] = $('.js-range-amount-7').text();     // Общая стоимость такого имущества в собственности супруга должника Stns
    //data_form['STRS'] = $('.js-range-amount-8').text();     // Общая стоимость транспорта в собственности супруга Strs

    $.ajax({
        url : '/include/calcul_result.php',
        type : 'POST',
        data : data_form,
        success : function (msg){
            if (msg){
                //console.info(msg);
                $('.container.container_md.result').html(msg);
                //$('.section.section_bg_gray.calculator').after(msg);
            }
        }
    });
});

$(document).on('click', '#reload', function(){
    location.reload();
});

jQuery(document).ready(function($)
{
    $('#opacity').css('display', 'none');
    
    $(document).on('click', '.form__choice label',function() 
    {
        var togglename = $(this).data('check'), toggleid = $(this).data('checked');
        $("."+togglename).addClass('invise');
        $("#"+toggleid).removeClass('invise');
    });

    $('[name="sdelkiNDI"]').change(function(){changeSdelki($(this))});
    $('[name="sdelkiDI"]').change(function(){changeSdelki($(this))});
    
    $('[name="sdelkiNDIS"]').change(function(){changeSdelki($(this))});
    $('[name="sdelkiDIS"]').change(function(){changeSdelki($(this))});

//    $('#amountDeals').change(function() 
//    {
//        if ($(this).val() != '0')
//        {
//            changeSdelki($(this));
            
//            $.ajax
//            ({
//                url : '/include/calcul_sdelki.php',
//                type : 'POST',
//                data : { 'sdelki': $(this).val() , 'typeSd': $(this).data('deal') },
//                success : function (data)
//                {
//                    if (data)
//                    {
//                    $('#'+$(this).attr('name')).html(data);
//                    $('.js-select').selectpicker({size: 'auto',}); // обновить верстку для селектов
//                    }
//                }
//            });
//        } 
//        else 
//        {
//            console.log('очистить');
//            $("#"+$(this).attr('name')).html('');
//        }
//    });
    
//    $('[name="sdelkiNDI"]').change(function() 
//    {
//        if ($(this).val() != '0')
//        {
//            $.ajax
//            ({
//                url : '/include/calcul_sdelki.php',
//                type : 'POST',
//                data : { 'sdelki': $(this).val() , 'typeSd': 'NDI' },
//                success : function (data)
//                {
//                    if (data)
//                    {
//                    $('#sdelkiNDI').html(data);
//                    $('.js-select').selectpicker({size: 'auto',}); // обновить верстку для селектов
//                    }
//                }
//            });
//        } 
//        else 
//            {
//                console.log('очистить');
//                $("#sdelkiNDI").html('');
//            }
//    });
    
//    $('[name="sdelkiDI"]').change(function() 
//    {
//        if ($(this).val() != '0')
//        {
//            $.ajax
//            ({
//            url : '/include/calcul_sdelki.php',
//            type : 'POST',
//            data : { 'sdelki': $(this).val() , 'typeSd': 'DI' },
//            success : function (data)
//            {
//                if (data)
//                {
//                //console.info(data);
//                $('#sdelkiDI').html(data);
//                $('.js-select').selectpicker({size: 'auto',}); // обновить верстку для селектов
//                }
//            }
//            });
//        } 
//        else 
//        {
//            console.log('очистить');
//            $("#sdelkiDI").html('');
//        }
//    });

});

//
$(document).on('click', '#holost', function()
{
    $('#opacity').slideUp();
});

$(document).on('click', '#brak', function()
{
    $('#opacity').slideDown();
});

$(document).on('click', '#region_save', function()
{
    var region = {};
    region['region'] = $('[name="REG"]').val();
    $.ajax({
        url : '/include/calcul_region.php',
        type : 'POST',
        data : region,
        success : function (msg){
            if (msg){
                $('#test').html(msg);
                //$('.section.section_bg_gray.calculator').after(msg);
            }
        }
    });
});

