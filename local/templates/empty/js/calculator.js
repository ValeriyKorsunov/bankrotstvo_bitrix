jQuery(document).ready(function($){
  $('.calculator select').change(function() {
    var toggleline = $(this).data('hide');;
    if ($(this).val() != '0') {
      $("."+toggleline).removeClass('invise');
    } else {
      $("."+toggleline).addClass('invise');
    }
  });

  // $('.form__choice label').click(function() {
  //   alert('.form__choice label');
  //   var togglename = $(this).data('check'), toggleid = $(this).data('checked');
  //   $("."+togglename).addClass('invise');
  //   $("#"+toggleid).removeClass('invise');
  // });

  $(document).click(function(e) {
      var target = e.target;
      if (!$(target).is($('.tooltip__toggle'))) {
          $('.tooltip').removeClass('active');
          $('.tooltip__body').fadeOut();
      }
  });
    
    // Select
    $('.js-select').selectpicker({
        size: '6',
    });

    // Sliders
    $('.js-calculator-slider').slick({
        infinite:false,
        arrows: true,
        fade:true,
        prevArrow: $('.js-calculator-slider-prev'),
        nextArrow: $('.js-calculator-slider-next'),
    });
    $(".calculator__steps .steps__item").on('click', function() {
        slideIndex = $(this).index();
        $('.js-calculator-slider').slick('slickGoTo', parseInt(slideIndex));
    });
    $(window).on('orientationchange resize', function() {
        $('.js-calculator-slider').slick('resize');
    });
    
    // Tooltip
    $('.tooltip__toggle').click(function() {
        $('.tooltip').removeClass('active');
        $('.tooltip__body').fadeOut();
        $(this).parent().toggleClass('active');
        $(this).next().fadeToogle();
    });

    $('.tooltip__close').click(function(e) {
        e.preventDefault();
        $(this).parents('.tooltip').removeClass('active');
        $(this).parent().fadeOut();
    });


    $( ".js-range-slider-1" ).slider({
      range: "max",
      min: 200000,
      max: 1000000000,
      value: 300000,
      step: 10000,
      slide: function( event, ui ) {
        $( ".js-range-amount-1" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-1" ).html( $( ".js-range-slider-1" ).slider( "value" ).toLocaleString() );

    $( ".js-range-slider-2" ).slider({
      range: "max",
      min: 0,
      max: 200000,
      value: 15000,
      slide: function( event, ui ) {
        $( ".js-range-amount-2" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-2" ).html( $( ".js-range-slider-2" ).slider( "value" ).toLocaleString() );

    $( ".js-range-slider-3" ).slider({
      range: "max",
      min: 0,
      max: 100000,
      value: 0,
      slide: function( event, ui ) {
        $( ".js-range-amount-3" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-3" ).html( $( ".js-range-slider-3" ).slider( "value" ).toLocaleString() );

    $( ".js-range-slider-4" ).slider({
      range: "max",
      min: 0,
      max: 3000000,
      value: 0,
      slide: function( event, ui ) {
        $( ".js-range-amount-4" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-4" ).html( $( ".js-range-slider-4" ).slider( "value" ).toLocaleString() );

    $( ".js-range-slider-5" ).slider({
      range: "max",
      min: 0,
      max: 5000000,
      value: 0,
      slide: function( event, ui ) {
        $( ".js-range-amount-5" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-5" ).html( $( ".js-range-slider-5" ).slider( "value" ).toLocaleString() );

    $( ".js-range-slider-6" ).slider({
      range: "max",
      min: 0,
      max: 5000000,
      value: 0,
      slide: function( event, ui ) {
        $( ".js-range-amount-6" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-6" ).html( $( ".js-range-slider-6" ).slider( "value" ).toLocaleString() );


    $( ".js-range-slider-7" ).slider({
      range: "max",
      min: 0,
      max: 100000,
      value: 50000,
      slide: function( event, ui ) {
        $( ".js-range-amount-7" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-7" ).html( $( ".js-range-slider-7" ).slider( "value" ).toLocaleString() );


    $( ".js-range-slider-8" ).slider({
      range: "max",
      min: 0,
      max: 100000,
      value: 50000,
      slide: function( event, ui ) {
        $( ".js-range-amount-8" ).html( ui.value.toLocaleString() );
      }
    });
    $( ".js-range-amount-8" ).html( $( ".js-range-slider-8" ).slider( "value" ).toLocaleString() );
});
