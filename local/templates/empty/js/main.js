function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var locationName = getCookie("location");
    if (locationName != "") {
        $('.js-location-toggle').html(locationName);
        $('.dropdown-menu li.active').removeClass('active');
        $('.dropdown-menu li a:contains('+locationName+')').parent().addClass('active');
        var locationPhone = $('.dropdown-menu li.active a').attr('data-phone'),
            locationPhoneUrl = $('.dropdown-menu li.active a').attr('data-phoneurl'),
            locationEmail = $('.dropdown-menu li.active a').attr('data-email');
        $('.js-location-email').text(locationEmail).attr('href','mailto:'+locationEmail);
        $('.js-location-phone').text(locationPhone).attr('href','tel:'+locationPhoneUrl);
    }
}


jQuery(document).ready(function($){
    // Cookies
    checkCookie();
  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[rel]')
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[class*="more"]')
    .not('[class*="hide"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
        }
      }
  });

    // Nav
    var $navToggle = $('.js-nav-toggle'),
        $nav = $('.js-nav');

    $navToggle.click(function(){
        $nav.slideToggle();
    });

    if ($(window).width() < 992){
        $(document).click(function(e) {
            var target = e.target;
            if (!$(target).is($navToggle)) {
                $('.js-nav').slideUp();
            }
        });
    }

    // Masked Input
    $('.js-tel').mask("+7(999)999-99-99");
    
    // Custom control
    $(".custom-control__input").prop("indeterminate", true);
    
    // Truncatable FAQ text
    $('.faq__item-text').truncatable({
        limit: 530, 
        more: 'Читать полностью', 
        less: true, 
        hideText: 'Cкрыть' 
    });
    
    $('body').magnificPopup({
        delegate: 'a[rel="gallery"]',
        type: 'image',
        fixedContentPos: false,
        tLoading: 'Loading image #%curr%...',
        mainClass: 'my-mfp-zoom-in',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });

    $('a[rel="doc"]').magnificPopup({
        type: 'iframe',
        fixedContentPos: false,
        tLoading: 'Loading image #%curr%...',
        mainClass: 'my-mfp-zoom-in',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });
    
    $('a[rel="review"]').magnificPopup({
        type: 'image',
        fixedContentPos: false,
        closeOnContentClick: true,
        mainClass: 'my-mfp-zoom-in',
    });

    $('.dropdown-menu__location a').on('click', function(e) {
        if(!$(this).parent().hasClass('active')) {
            $(this).parent().siblings().removeClass('active');
            $(this).parent().addClass('active');
            var locationActive = $('.dropdown-menu__location li.active a').text(),
                locationPhone = $('.dropdown-menu__location li.active a').attr('data-phone'),
                locationPhoneUrl = $('.dropdown-menu__location li.active a').attr('data-phoneurl'),
                locationEmail = $('.dropdown-menu__location li.active a').attr('data-email');
            $('.js-location-toggle').html(locationActive);
            $('.js-location-email').text(locationEmail).attr('href','mailto:'+locationEmail);
            $('.js-location-phone').text(locationPhone).attr('href','tel:'+locationPhoneUrl);
            setCookie("location", locationActive, 365);
        }
    });

    // Hero Slider
    $('.js-hero-slider').on('init', function(event, slick) {
        var itemBg = $(this).find('.hero__item.slick-current').attr('data-bg');
        $('.js-hero-img').attr('style', itemBg);
    });

    $('.js-hero-slider').slick({
        infinite: true,
        autoplay: true,
        speed: 300,
        autoplaySpeed: 10000,
        arrows: true,
        dots: true,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                infinite: true,
                autoplay: true,
                arrows: false,
                dots: true,
            }
        }]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        var itemBg = $(slick.$slides[nextSlide]).attr('data-bg');
        $('.js-hero-img').attr('style', itemBg);
    });

    $(window).on('orientationchange resize', function() {
        $('.js-hero-slider').slick('resize');
    });

    //to top

    var top_show = 150; 
    var delay = 1000;
    $(window).scroll(function(){
        if ($(window).scrollTop() > 100) {
            $('#totop').fadeIn('300');
        }
        else {
            $('#totop').fadeOut('300');
        }
    }); 

    $('#contact_form').on('submit', function(e){
        e.preventDefault();
        var $that = $(this),
            formData = new FormData($that.get(0));

        $.ajax({
            url : '/send_mail.php',
            type : 'POST',
            processData: false,
            contentType: false,
            cache:false,
            data : formData,
            success : function (msg){
                if (msg){
                    if (msg == 'error'){

                        $('#error').addClass('form__error');
                        $('#error').html("Ошибка. Сообщение не отправленно.<br>");
                        return;
                    }
                    $('.feedback__form.form').addClass('hidden');

                    $('.section_bg_black.feedback .container').html('<h2 class="m0 mt10">Спасибо, ваше сообщение принято.</h2>');
                }
            }
        });
    });
});
