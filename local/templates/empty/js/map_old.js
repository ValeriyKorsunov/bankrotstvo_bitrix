// hack Google Maps to bypass API v3 key (needed since 22 June 2016 http://googlegeodevelopers.blogspot.com.es/2016/06/building-for-scale-updates-to-google.html)
var target = document.head;
var observer = new MutationObserver(function(mutations) {
    for (var i = 0; mutations[i]; ++i) { // notify when script to hack is added in HTML head
        if (mutations[i].addedNodes[0].nodeName == "SCRIPT" && mutations[i].addedNodes[0].src.match(/\/AuthenticationService.Authenticate?/g)) {
            var str = mutations[i].addedNodes[0].src.match(/[?&]callback=.*[&$]/g);
            if (str) {
                if (str[0][str[0].length - 1] == '&') {
                    str = str[0].substring(10, str[0].length - 1);
                } else {
                    str = str[0].substring(10);
                }
                var split = str.split(".");
                var object = split[0];
                var method = split[1];
                window[object][method] = null; // remove censorship message function _xdc_._jmzdv6 (AJAX callback name "_jmzdv6" differs depending on URL)
                //window[object] = {}; // when we removed the complete object _xdc_, Google Maps tiles did not load when we moved the map with the mouse (no problem with OpenStreetMap)
            }
            observer.disconnect();
        }
    }
});
var config = { attributes: true, childList: true, characterData: true }
observer.observe(target, config);

/*
 * 5 ways to customize the infowindow
 * 2015 - en.marnoto.com
*/

// map center
var center = new google.maps.LatLng(48.802271, 44.745845);

// marker position
var locations = [
  ['Центральный офис', 48.793183, 44.750201, '<div class="contacts__popup"><h4 class="map__title">Центральный Офис</h4><p>г. Волгоград <br> ул. Ленина, д.12, офис 007</p><p><a href="tel:+78442235323" class="tel">+7 (8442) 23-53-23</a> <br> <a href="mailto:info@bankrotstvoGrazdan.ru" class="link link_black">info@bankrotstvoGrazdan.ru</a></p><p><a href="">Подробнее</a></p></div>'],
  ['Дополнительный офис 1', 48.802568, 44.742733, '<div class="contacts__popup"><h4 class="map__title">Дополнительный офис 1</h4><p>г. Волгоград <br> ул. Ленина, д.12, офис 007</p><p><a href="tel:+78442235323" class="tel">+7 (8442) 23-53-23</a> <br> <a href="mailto:info@bankrotstvoGrazdan.ru" class="link link_black">info@bankrotstvoGrazdan.ru</a></p><p><a href="">Подробнее</a></p></div>'],
  ['Дополнительный офис 2', 48.789451, 44.740159, '<div class="contacts__popup"><h4 class="map__title">Дополнительный офис 2</h4><p>г. Волгоград <br> ул. Ленина, д.12, офис 007</p><p><a href="tel:+78442235323" class="tel">+7 (8442) 23-53-23</a> <br> <a href="mailto:info@bankrotstvoGrazdan.ru" class="link link_black">info@bankrotstvoGrazdan.ru</a></p><p><a href="">Подробнее</a></p></div>'],
];

var markers = [];
var map;

function initialize() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: center,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var iconBase = '../i/';
  var defaultIcon = iconBase + 'pin.svg';
  var activeIcon = iconBase + 'pin-active.svg';
  var marker, i;

  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: defaultIcon,
      html: locations[i][3]
    });

    // A new Info Window is created and set content
    var infowindow = new google.maps.InfoWindow({
      content: locations[i][3],
      pixelOffset: new google.maps.Size(282,246),

      // Assign a maximum value for the width of the infowindow allows
      // greater control over the various content elements
        maxWidth: 400
    });
    
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(this.html);
        infowindow.open(map, marker);
        for (var j = 0; j < markers.length; j++) {
          markers[j].setIcon(defaultIcon);
        }
        marker.setIcon(activeIcon);
      };
    })(marker, i));
    markers.push(marker);
  }
  
  // Event that closes the Info Window with a click on the map
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });
  
  google.maps.event.addListener(infowindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');

    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();

    iwBackground.css({'top' : '10px'});

    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    // Removes white background DIV
    iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    // Remones the shadow.
    iwBackground.children(':nth-child(1)').css({'display':'none'});

    // Changes the desired tail shadow color.
    iwBackground.children(':nth-child(3)').find('div').children().css({'display': 'none'});
  

    // Reference to the div that groups the close button elements.
    var iwCloseBtn = iwOuter.next();

    // Apply the desired effect to the close button
    iwCloseBtn.css({display: 'none'});

    // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
    if($('.iw-content').height() < 140){
      $('.iw-bottom-gradient').css({display: 'none'});
    }

    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
    });
  });
}
google.maps.event.addDomListener(window, 'load', initialize);